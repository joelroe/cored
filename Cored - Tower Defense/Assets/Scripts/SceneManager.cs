﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneManager : MonoBehaviour
{
    public GameObject[] waypoints;
    public LineRenderer[] Lines;
    public GameObject Enemy1, Enemy2, Enemy3;
    public float spawnDelay;

    private bool canSpawn = true;

    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < 11; i++)
        {
            Lines[i].SetPosition(0, waypoints[i].transform.position);
            Lines[i].SetPosition(1, waypoints[i+1].transform.position);
            Lines[i].enabled = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (canSpawn)
        {
            Spawn();
        }
    }

    public int howManyPoints()
    {
        return waypoints.Length;
    }

    public GameObject fetchWaypoint(int i)
    {
        return waypoints[i];
    }

    private void Spawn()
    {
        float rand = Random.Range(0f, 1f);
        GameObject temp;
        if (rand < .7f)
        {
            temp = GameObject.Instantiate(Enemy1);
        }
        else if (rand < .9f)
        {
            temp = GameObject.Instantiate(Enemy2);
        }
        else
        {
            temp = GameObject.Instantiate(Enemy3);
        }      
        temp.transform.position = waypoints[0].transform.position;
        canSpawn = false;
        StartCoroutine(canSpawnTimer());
    }

    IEnumerator canSpawnTimer()
    {
        yield return new WaitForSeconds(spawnDelay);
        canSpawn = true;
    }
}
