﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    public Sprite normal; // -----
    public Sprite slight;//  stuff for sprite damage
    private SpriteRenderer spriteRenderer; //-----
    public Sprite critical;// ---
    public int maxhp;//---
    private float percent;

    public GameObject[] waypoints;
    public float enemySpeed = 5;
    public int health;
    int currentWaypoint = 0;

    public Vector2 dir;
    private GameObject currentWaypointTarget = null;
    private SceneManager sceneManagerScript;

    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>(); // we are accessing the SpriteRenderer that is attached to the Gameobject
        if (spriteRenderer.sprite == null) // if the sprite on spriteRenderer is null then
            spriteRenderer.sprite = normal; // set the sprite to sprite1

        GameObject sceneManager = GameObject.FindGameObjectWithTag("SceneManager");
        sceneManagerScript = sceneManager.GetComponent<SceneManager>();
        fetchWaypoints();
    }

    public int findClosestIndex()
    {
        int index = 0;
        float distance = 100;
        for (int i = 0; i < waypoints.Length; i++)
        {
            Vector3 temp = waypoints[i].transform.position - this.transform.position;
            Debug.Log("Index - " + index + " - " + temp.magnitude);
            if (temp.magnitude < distance)
            {
                index = i;
                distance = temp.magnitude;
            }
        }

        Debug.Log("Closest Index - " + index);

        return index;
    }

    public Vector3 evaluateWaypoint()
    {
        dir = waypoints[currentWaypoint].transform.position -
            this.transform.position;
        if (dir.magnitude < 0.1f)
        {
            currentWaypoint++;
            if (currentWaypoint >= waypoints.Length)
            {
                Destroy(this.gameObject);
            }
        }

        dir.Normalize();

        return dir;
    }

    public void Hit(int damage)
    {
        health -= damage;
        percent = health / maxhp;
        if ((percent <= .75 ) && (percent >= .41))
        {
            spriteRenderer.sprite = slight;
        }
        if (percent <= .40)
        {
            spriteRenderer.sprite = critical;
        }
        if (health < 0)
        {
            Die();
        }
    }

    private void Die()
    {
        Destroy(this.gameObject);
    }

    private void fetchWaypoints()
    {
        int temp = sceneManagerScript.howManyPoints();
        waypoints = new GameObject[temp];
        for (int i = 0; i < temp; i++)
        {
            waypoints[i] = sceneManagerScript.fetchWaypoint(i);
        }
    }

    private void findClosest()
    {
        float shortestDistance = float.MaxValue;
        Vector3 shortestDirection = Vector2.zero;
        Vector3 dirTemp = Vector3.zero;
        foreach (GameObject g in waypoints)
        {
            dirTemp = g.transform.position - this.transform.position;
            dirTemp.y = 0;
            float distance = dirTemp.magnitude;
            if (distance <= 0.6)
                continue;
            if (distance < shortestDistance)
            {
                dirTemp.y = 0;
                shortestDirection = dirTemp;
                shortestDistance = distance;
                currentWaypointTarget = g;
            }
        }

        dir = shortestDirection;
    }
    public Vector2 findClosestWaypoint()
    {

        Vector3 foo = this.transform.position -
            currentWaypointTarget.transform.position;
        float distanceToTarget = foo.magnitude;

        if (distanceToTarget < 0.6)
        {
            findClosest();
        }

        return dir.normalized;
    }
    // Update is called once per frame
    void Update()
    {
        Vector3 dir = Vector3.zero;
        float localSpeed = 0;
        dir = evaluateWaypoint();
        //Debug.Log(dir);
        localSpeed = enemySpeed;
        //Debug.Log(localSpeed);
        this.transform.position += dir * localSpeed * Time.deltaTime;
    }
}
