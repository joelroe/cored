﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretShooting: MonoBehaviour
{
    public GameObject shootPoint;
    public LineRenderer shootLine;
    //public Light gunlight;
    public int damage = 10;

    //public int clipsize, roundsInClip, penetration;
    public float shotDelay, range, rotateSpeed;

    //public AudioSource shootSound, reloadSound;
    //public AudioClip shoot, reload;

    private bool canShoot = true, reloading = false;
    //private SceneManagerMain sceneManagerScript;
    private GameObject aimTarget;
    // Start is called before the first frame update
    void Start()
    {
        //shootSound.clip = shoot;
        //reloadSound.clip = reload;
        //gunlight.enabled = false;
        //GameObject sceneManager = GameObject.FindGameObjectWithTag("SceneManagerMain");
        //sceneManagerScript = sceneManager.GetComponent<SceneManagerMain>();
        //sceneManagerScript.updateClip(clipsize, clipsize);
    }

    // Update is called once per frame
    void Update()
    {
        ////Debug.Log("Update");
        //if (Input.GetMouseButtonDown(0) && roundsInClip > 0 && canShoot && !reloading)
        //{
        //    //Debug.Log("Pressed");
        //    Shoot();
        //}

        //if (roundsInClip < clipsize / 4f && !reloading)
        //{
        //    sceneManagerScript.updateNotifications("Press \"R\" to Reload");
        //}

        //if ((roundsInClip == 0 || (Input.GetKey(KeyCode.R) && roundsInClip < clipsize)) && !reloading)//Reloads when clip is empty or when user presses R while clip is not full
        //{
        //    reloading = true;
        //    Reload();
        //}
        if(findTarget() && canShoot)
        {          
            Shoot();
        }
    
    }

    private bool findTarget()
    {
        GameObject[] existingEnemies;
        GameObject currentTarget = null;
        Vector3 shortest = Vector3.zero;
        existingEnemies = GameObject.FindGameObjectsWithTag("Enemy");

        foreach(GameObject enemy in existingEnemies)
        {
            Vector3 temp = enemy.transform.position - this.transform.position;
            if (temp.sqrMagnitude < range*range)
            {
                if(currentTarget == null)
                {
                    currentTarget = enemy;
                    shortest = temp;
                }
                else if (temp.sqrMagnitude < shortest.sqrMagnitude)
                {
                    currentTarget = enemy;
                    shortest = temp;
                }
            }
        }

        if(currentTarget == null)
        {
            return false;
        }
        else
        {
            aimTarget = currentTarget;
            faceTarget();
            return true;
        }
    }

    private void faceTarget()
    {
        Vector3 vectorToTarget = aimTarget.transform.position - transform.position;
        float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;
        Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
        transform.rotation = Quaternion.Slerp(transform.rotation, q, Time.deltaTime * rotateSpeed);
    }

    private void Shoot()
    {
        doDamage();
        shootLine.SetPosition(0, shootPoint.transform.position);
        shootLine.SetPosition(1, aimTarget.transform.position);
        shootLine.enabled = true;
        canShoot = false;
        StartCoroutine(turnOffEffects());
        StartCoroutine(EnableShooting());
        ////roundsInClip--;
        ////shootSound.Play();
        ////sceneManagerScript.updateClip(roundsInClip, clipsize);
        //RaycastHit2D[] hits = Physics2D.RaycastAll(shootPoint.transform.position, shootPoint.transform.right * 20);
        ////Debug.DrawRay(shootPoint.transform.position, shootPoint.transform.right * 10, Color.red, 20, true);
        //for (int i = 0; i < hits.Length; i++)
        //{
        //    RaycastHit2D hit = hits[i];
        //    if (hit && i < penetration)
        //    {
        //        if (hit.collider.gameObject.transform.name.EndsWith("Zombie(Clone)"))
        //        {
        //            hit.collider.gameObject.GetComponent<ZombieMovement>().hit(damage);
        //        }
        //    }
        //}

        //if (hits.Length >= penetration
        //    && hits[penetration - 1] != null)
        //{
        //    shootLine.SetPosition(0, shootPoint.transform.position);
        //    shootLine.SetPosition(1, aimPoint);
        //}
        //else
        //{
        //    shootLine.SetPosition(0, shootPoint.transform.position);
        //    shootLine.SetPosition(1, shootPoint.transform.position + shootPoint.transform.right * 100);
        //}

        //shootLine.enabled = true;
        //gunlight.enabled = true;
        //canShoot = false;

        //StartCoroutine(turnOffEffects());
        //StartCoroutine(EnableShooting());
    }

    private void doDamage()
    {
        EnemyMovement enemyScript = aimTarget.GetComponent<EnemyMovement>();
        enemyScript.Hit(damage);
    }

    IEnumerator turnOffEffects()
    {
        yield return new WaitForSeconds(0.05f);

        shootLine.enabled = false;
        //gunlight.enabled = false;
    }

    IEnumerator EnableShooting()
    {
        yield return new WaitForSeconds(shotDelay);

        canShoot = true;
    }

    //private void Reload()
    //{
    //    reloadSound.Play();
    //    sceneManagerScript.updateNotifications("Reloading...");
    //    StartCoroutine(handleReload());
    //}

    //IEnumerator handleReload()
    //{
    //    yield return new WaitForSeconds(reloadDelay);

    //    roundsInClip = clipsize;
    //    reloading = false;

    //    sceneManagerScript.updateNotifications("");
    //    sceneManagerScript.updateClip(clipsize, clipsize);
    //}
}
