﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingleSpawner : MonoBehaviour
{
    public GameObject SingleTurret;
    public GameObject MultiTurret;
    public GameObject SniperTurret;
    public GameObject GatlingTurret;
    public GameObject Cannon;

    private bool press = false;
    private int turretpick = 1;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            if (press == true)
            {
                switch (turretpick)
                {
                    case 1:
                        singlespawn();
                        break;
                    case 2:
                        multispawn();
                        break;
                    case 3:
                        sniperspawn();
                        break;
                    case 4:
                        gatlingspawn();
                        break;
                    case 5:
                        cannonspawn();
                        break;
                    default:
                        press = false;
                        break;
                }
            }
        }
    }

    public void singlepress()
    {
        turretpick = 1;
        press = true;
    }

    public void singlespawn()
    {
        Vector3 worldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition, Camera.MonoOrStereoscopicEye.Mono);

        // make z position the Z position of the pefab object
        Vector3 adjustZ = new Vector3(worldPoint.x, worldPoint.y, SingleTurret.transform.position.z);

        Spawn(adjustZ, SingleTurret);

        press = false;
    }

    public void multipress()
    {
        turretpick = 2;
        press = true;
    }

    public void multispawn()
    {
        Vector3 worldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition, Camera.MonoOrStereoscopicEye.Mono);

        // make z position the Z position of the pefab object
        Vector3 adjustZ = new Vector3(worldPoint.x, worldPoint.y, MultiTurret.transform.position.z);

        Spawn(adjustZ, MultiTurret);

        press = false;
    }

    public void sniperpress()
    {
        turretpick = 3;
        press = true;
    }

    public void sniperspawn()
    {
        Vector3 worldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition, Camera.MonoOrStereoscopicEye.Mono);

        // make z position the Z position of the pefab object
        Vector3 adjustZ = new Vector3(worldPoint.x, worldPoint.y, SniperTurret.transform.position.z);

        Spawn(adjustZ, SniperTurret);

        press = false;
    }

    public void gatlingpress()
    {
        turretpick = 4;
        press = true;
    }

    public void gatlingspawn()
    {
        Vector3 worldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition, Camera.MonoOrStereoscopicEye.Mono);

        // make z position the Z position of the pefab object
        Vector3 adjustZ = new Vector3(worldPoint.x, worldPoint.y, GatlingTurret.transform.position.z);

        Spawn(adjustZ, GatlingTurret);

        press = false;
    }

    public void cannonpress()
    {
        turretpick = 5;
        press = true;
    }

    public void cannonspawn()
    {
        Vector3 worldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition, Camera.MonoOrStereoscopicEye.Mono);

        // make z position the Z position of the pefab object
        Vector3 adjustZ = new Vector3(worldPoint.x, worldPoint.y, Cannon.transform.position.z);

        Spawn(adjustZ, Cannon);

        press = false;
    }

    public void Spawn(Vector3 position, GameObject spawnee)
    {
        Instantiate(spawnee).transform.position = position;
    }
}
